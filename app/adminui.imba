import { IndividualPlayerStore, SharedStore, CharacterFrame, Song, Room } from '../src/types'
import { SONG_TYPE, CHARACTER_TYPE, SONGS, ROOMS, CHARACTER_FRAMES } from './loader'
import { PAGE_STATUS, receiveWsMessage, sendWsMessage } from './helpers'
import { ClientMessage, CLIENT_MSG_TYPE, SERVER_MSG_TYPE } from '../src/wss/messageTypes'
import axios from 'axios'
import capitalize from 'capitalize'

tag AdminUi
	ws\WebSocket
	pageStatus\string
	token\string

	def routed params
		pageStatus = PAGE_STATUS.LOADING
		token = params.token
		render!

		axios
		.get("/api/verify/admin/{params.token}")
		.then(do()
			ws = new WebSocket("ws://{window.location.hostname}:30030/?token={params.token}")
			pageStatus = PAGE_STATUS.LOADED
		)
		.catch(do()
			pageStatus = PAGE_STATUS.ERROR #
		)
	
	def render
		<self>
			if pageStatus === PAGE_STATUS.LOADED
				<AdminUiContent ws=ws token=token>
			elif pageStatus === PAGE_STATUS.ERROR
				<p> "Error"


tag AdminUiContent
	prop ws\WebSocket
	prop token\string

	connectedClients = []

	sharedState\SharedStore
	individualStates\IndividualPlayerStore[]

	def mount
		ws.onopen = do()
			sendWsMessage ws, new ClientMessage
				type: CLIENT_MSG_TYPE.GET_SHARED_STATE
				senderToken: token
			
			sendWsMessage ws, new ClientMessage
				type: CLIENT_MSG_TYPE.GET_EVERY_INDIVIDUAL_STATES
				senderToken: token
				data: token

		ws.onmessage = do(rawMessage)
			const message = receiveWsMessage(rawMessage.data)

			if message.type === SERVER_MSG_TYPE.CLIENT_CONNECTION
				connectedClients = message.data
			else if message.type === SERVER_MSG_TYPE.UPDATE_SHARED_STATE
				sharedState = new SharedStore message.data
			else if message.type === SERVER_MSG_TYPE.UPDATE_EVERY_INDIVIDUAL_STATES
				individualStates = message.data.map do(state) new IndividualPlayerStore state

			imba.commit!

	css .btn-container w:100% ta:center
	css .btn w:100% fs:22px
	css .btn-custom bgc:gray2 bgc@hover:blue4
	css .col-4 pb:10px
	css .col-2 pb:10px
	css .col-1 pb:5px
	css .row pb:25px
	css html ofy:visible

	def render
		<self>
			for client in connectedClients
				<p> client.id

			<div.row>
				<h2> "Players"

				const noCharacterMessage = new ClientMessage 
					type: CLIENT_MSG_TYPE.DISPLAY_CHARACTER
					senderToken: token
					data: null

				<div.col-2>
					<div.btn-container> 
						<button.btn.btn-custom @click=sendWsMessage(ws, noCharacterMessage)> "None"

				for characterFrame\CharacterFrame in Object.keys(CHARACTER_FRAMES).map(do(obj) CHARACTER_FRAMES[obj]).filter(do(obj) obj.type === CHARACTER_TYPE.PLAYER)
					<div.col-2>
						<CharacterButton ws=ws token=token characterFrame=characterFrame>

			<div.row>
				<h2> "Monokuma"

				for characterFrame\CharacterFrame in Object.keys(CHARACTER_FRAMES).map(do(obj) CHARACTER_FRAMES[obj]).filter(do(obj) obj.type === CHARACTER_TYPE.MONOKUMA)
					<div.col-2>
						<CharacterButton ws=ws token=token characterFrame=characterFrame>

			<div.row>
				for room\Room in Object.keys(ROOMS).map(do(obj) ROOMS[obj])
					<div.col-2>
						<RoomButton ws=ws token=token room=room>

			<div.row>
				<h2> "🎵 Normal"

				const noSongMessage = new ClientMessage 
					type: CLIENT_MSG_TYPE.CHANGE_MUSIC
					senderToken: token
					data: null

				<div.col-2>
					<div.btn-container> 
						<button.btn.btn-custom @click=sendWsMessage(ws, noSongMessage)> "🎵 None"

				for song\Song in Object.keys(SONGS).map(do(obj) SONGS[obj]).filter(do(obj) obj.type === SONG_TYPE.NORMAL)
					<div.col-2>
						<MusicButton ws=ws token=token song=song>

			<div.row>
				<h2> "🎵 Special"
				for song\Song in Object.keys(SONGS).map(do(obj) SONGS[obj]).filter(do(obj) obj.type === SONG_TYPE.SPECIAL)
					<div.col-2>
						<MusicButton ws=ws token=token song=song>
			
			<div.row>
				<h2> "🎵 Class Trial"
				for song\Song in Object.keys(SONGS).map(do(obj) SONGS[obj]).filter(do(obj) obj.type === SONG_TYPE.TRIAL)
					<div.col-2>
						<MusicButton ws=ws token=token song=song>
			
			<div.row>
				if individualStates
					for individualState in individualStates
						<div.col-3>
							<PlayerPart ws=ws individualState=individualState token=token>
			
			<div.row>
				if sharedState
					<div.col-4>
						const message = new ClientMessage 
							type: CLIENT_MSG_TYPE.SWITCH_CASE_MODE
							senderToken: token
							data: !sharedState.inCase?

						<div.btn-container> 
							<button.btn.btn-custom @click=sendWsMessage(ws, message)> (sharedState.inCase? ? "Disable" : "Enable") + " case mode"

					<div.col-4>
						const message = new ClientMessage 
							type: CLIENT_MSG_TYPE.SWITCH_TRIAL_MODE
							senderToken: token
							data: !sharedState.inTrial?

						<div.btn-container> 
							<button.btn.btn-custom @click=sendWsMessage(ws, message)> (sharedState.inTrial? ? "Disable" : "Enable") + " trial mode" 

tag CharacterButton
	prop ws\WebSocket
	prop token\string
	prop characterFrame\CharacterFrame

	message\ClientMessage

	def mount
		message = new ClientMessage 
			type: CLIENT_MSG_TYPE.DISPLAY_CHARACTER
			senderToken: token
			data: new CharacterFrame
				file: (await characterFrame.file).url
				name: characterFrame.name

	css .btn-container w:100% ta:center
	css .btn w:100% fs:22px
	css .btn-custom bgc:gray2 bgc@hover:blue4

	def render
		<self>
			<div.btn-container> 
				<button.btn.btn-custom @click=sendWsMessage(ws, message)> "🧸 {characterFrame.name}"

tag RoomButton
	prop ws\WebSocket
	prop token\string
	prop room\Room

	message\ClientMessage

	def mount
		message = new ClientMessage 
			type: CLIENT_MSG_TYPE.CHANGE_ROOM
			senderToken: token
			data: new Room
				file: (await room.file).url
				name: room.name

	css .btn-container w:100% ta:center
	css .btn w:100% fs:22px
	css .btn-custom bgc:gray2 bgc@hover:blue4

	def render
		<self>
			<div.btn-container> 
				<button.btn.btn-custom @click=sendWsMessage(ws, message)> "🚪 {room.name}"

tag MusicButton
	prop ws\WebSocket
	prop token\string
	prop song\Song

	message\ClientMessage

	def mount
		message = new ClientMessage 
			type: CLIENT_MSG_TYPE.CHANGE_MUSIC
			senderToken: token
			data: new Song
				loopingFile: (await song.loopingFile).default if song.loopingFile
				file: (await song.file).default
				title: song.title

	css .btn-container w:100% ta:center
	css .btn w:100% fs:22px
	css .btn-custom bgc:gray2 bgc@hover:blue4

	def render
		<self>
			<div.btn-container> 
				<button.btn.btn-custom @click=sendWsMessage(ws, message)> "🎵 {song.title}"

tag PlayerPart
	prop individualState\IndividualPlayerStore
	prop ws\WebSocket
	prop token\string

	def disableMessage
		sendWsMessage(ws, new ClientMessage { senderToken: token, type: CLIENT_MSG_TYPE.DISABLE, data: individualState.id })
		sendWsMessage(ws, new ClientMessage { senderToken: token, type: CLIENT_MSG_TYPE.GET_EVERY_INDIVIDUAL_STATES })

	def enableMessage
		sendWsMessage(ws, new ClientMessage { senderToken: token, type: CLIENT_MSG_TYPE.ENABLE, data: individualState.id })
		sendWsMessage(ws, new ClientMessage { senderToken: token, type: CLIENT_MSG_TYPE.GET_EVERY_INDIVIDUAL_STATES })

	def sendMentalismScoreMessage e\Event
		sendWsMessage ws, new ClientMessage
			senderToken: token
			type: CLIENT_MSG_TYPE.CHANGE_MENTALISM_SCORE
			data: 
				target: individualState.id
				score: e.target.valueAsNumber
		sendWsMessage(ws, new ClientMessage { senderToken: token, type: CLIENT_MSG_TYPE.GET_EVERY_INDIVIDUAL_STATES })

	def sendSetTimerMessage
		sendWsMessage ws, new ClientMessage
			senderToken: token
			type: CLIENT_MSG_TYPE.SET_TIMER
			data: 
				target: individualState.id
				minutes: $timerInput.value
	
	def sendRemoveTimerMessage
		sendWsMessage ws, new ClientMessage
			senderToken: token
			type: CLIENT_MSG_TYPE.REMOVE_TIMER
			data: 
				target: individualState.id
	
	def sendStartTimerMessage
		sendWsMessage ws, new ClientMessage
			senderToken: token
			type: CLIENT_MSG_TYPE.START_TIMER
			data: 
				target: individualState.id
	
	def sendStopTimerMessage
		sendWsMessage ws, new ClientMessage
			senderToken: token
			type: CLIENT_MSG_TYPE.STOP_TIMER
			data: 
				target: individualState.id

	# TODO Timer

	css .btn-container w:100% ta:center
	css .btn w:100% fs:22px
	css .btn-custom bgc:gray2 bgc@hover:blue4

	def render
		<self>
			<div.row>
				<div.col-12>
					if individualState.enabled?
						<div.btn-container> 
							<button.btn.btn-danger @click=disableMessage> "Disconnect {individualState.name}"
					else 
						<div.btn-container> 
							<button.btn.btn-success @click=enableMessage> "Connect {individualState.name}"
			<div.row>
				<div.col-12>
					<div.btn-container>
						<input.form-control type="number" value=individualState.mentalismScore @change=sendMentalismScoreMessage>
			<div.row>
				<div.col-6>
					<div.btn-container>
						<input$timerInput.form-control type="number">	
				<div.col-6>
					<div.btn-container> 
						<button.btn.btn-primary @click=sendSetTimerMessage> "Set timer"

			<div.row>
				<div.col-6>
					<div.btn-container> 
						<button.btn.btn-success @click=sendStartTimerMessage> "Start timer"
				<div.col-6>
					<div.btn-container> 
						<button.btn.btn-danger @click=sendStopTimerMessage> "Stop timer"
				

			<div.row>
				<div.col-12>
					<div.btn-container> 
						<button.btn.btn-custom @click=sendRemoveTimerMessage> "Remove timer"

export {
	AdminUi
}
