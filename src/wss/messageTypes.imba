const SERVER_MSG_TYPE =
	UPDATE_SHARED_STATE: "update_shared_state"
	UPDATE_INDIVIDUAL_STATE: "update_individual_state"
	CLIENT_CONNECTION: "client_connection"
	UPDATE_EVERY_INDIVIDUAL_STATES: "update_every_individual_states"
	UPDATE_TIMER: "UPDATE_TIMER"

const CLIENT_MSG_TYPE =
	DISABLE: "disable"											# string
	ENABLE: "enable"											# string
	DISPLAY_CHARACTER: "display_character"						# CharacterFrame
	CHANGE_ROOM: "display_background"							# string
	GET_SHARED_STATE: "get_shared_state" 						# null
	GET_INDIVIDUAL_STATE: "get_individual_state" 				# string
	GET_EVERY_INDIVIDUAL_STATES: "get_every_individual_states" 	# null
	CHANGE_MUSIC: "change_music" 								# Song
	CHANGE_MENTALISM_SCORE: "change_mentalism_score" 			# { target:string, score:number }
	SWITCH_CASE_MODE: "SWITCH_CASE_MODE"						# bool
	SWITCH_TRIAL_MODE: "SWITCH_TRIAL_MODE"						# bool

	SET_TIMER: "SET_TIMER"										# { target:string, minutes:number }
	START_TIMER: "START_TIMER"									# { target:string }
	STOP_TIMER: "STOP_TIMER"									# { target:string }
	REMOVE_TIMER: "REMOVE_TIMER"								# { target:string }
	GET_TIMER: "GET_TIMER"										# { target:string }


class ClientMessage
	constructor {senderToken, type, data=null}
		this.senderToken\string = senderToken
		this.type\string = type
		this.data\any = data

class ServerMessage
	constructor {type, data, target}
		this.type\string = type
		this.data\any = data
		this.target\string = target

export {
	SERVER_MSG_TYPE
	CLIENT_MSG_TYPE
	ClientMessage
	ServerMessage
}
