class CharacterFrame
	constructor { file\string, name\string }
		this.file = file
		this.name = name

class Room
	constructor { file\string, name\string }
		this.file = file
		this.name = name

class Song
	constructor { loopingFile\string, file\string, title\string }
		this.loopingFile = loopingFile
		this.file = file
		this.title = title

class SharedStore
	constructor { background\Room, character\CharacterFrame, song\Song, inCase?\boolean, inTrial?\boolean, dynamicCharacterAnimation?\boolean }
		this.background = background
		this.character = character
		this.song = song
		this.inCase? = inCase?
		this.inTrial? = inTrial?
		this.dynamicCharacterAnimation? = dynamicCharacterAnimation?

class IndividualPlayerStore
	constructor { id\string, name\string, enabled?\boolean, mentalismScore\number, investigationTimerSeconds\number, investigationTimerInterval }
		this.id = id
		this.name = name
		this.enabled? = enabled?
		this.mentalismScore = mentalismScore
		this.investigationTimerSeconds = investigationTimerSeconds
		this.investigationTimerInterval = investigationTimerInterval

export {
	CharacterFrame
	Room
	Song
	SharedStore
	IndividualPlayerStore
}
