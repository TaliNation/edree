import { ServerMessage, ClientMessage } from '../src/wss/messageTypes'

const PAGE_STATUS =
	LOADED: "loaded"
	ERROR: "loading"
	LOADING: "loading"

def sendWsMessage ws\WebSocket, message\ClientMessage
	ws.send JSON.stringify message
	
def receiveWsMessage jsonStr\string
	new ServerMessage JSON.parse(jsonStr)

def triggerAnimation domObject, animation, timeInSeconds = 1, style = "linear"
	domObject.style.animation = ""
	setTimeout((do() domObject.style.animation = "{animation} {timeInSeconds}s {style}"), 10)

def areEqual(obj1, obj2)
	JSON.stringify(obj1) === JSON.stringify(obj2)

export {
	PAGE_STATUS
	sendWsMessage
	receiveWsMessage
	triggerAnimation
	areEqual
}
