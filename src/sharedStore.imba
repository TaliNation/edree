import { SharedStore, Room, CharacterFrame, Song } from './types'

const defaultStore = new SharedStore
	background: "dark"
	character: null
	song: null
	inCase?: false
	inTrial?: false
	dynamicCharacterAnimation?: false

const store = new SharedStore
	background: "dark"
	character: null
	song: null
	inCase?: false
	inTrial?: false
	dynamicCharacterAnimation?: false

def get
	JSON.parse(JSON.stringify(store))

def getDefault
	JSON.parse(JSON.stringify(defaultStore))

def setBackground background\Room
	store.background = background

def setCharacter character\CharacterFrame
	store.character = character

def setSong song\Song
	store.song = song

def setInCaseMode bool
	store.inCase? = bool

def setInTrialMode bool
	store.inTrial? = bool

export {
	get
	getDefault
	setBackground
	setCharacter
	setSong
	setInCaseMode
	setInTrialMode
}
