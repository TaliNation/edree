import { AdminUi } from './adminui'
import { PlayerUi } from './playerui'

global css html overflow: hidden
global css body margin:0 ff:'Shippori Antique B1', 'Segoe UI', sans-serif

global css @keyframes fade-in
	from opacity:0
	to opacity:1

global css @keyframes soft-appearance
	0% pt:30px pb:0
	100% pt:0 pb:30px

global css @keyframes hard-appearance-left
	0% pl:100%
	2% pl:100px
	100% pl:0

global css @keyframes hard-appearance-right
	0% pr:100%
	2% pr:100px
	100% pr:0

tag app
	<self>
		<PlayerUi route="/play/:token$">
		<AdminUi route="/control-panel/:token$">

imba.mount <app>
