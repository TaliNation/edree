import express from 'express'
import index from '../../app/index.html'
import { getPlayersId, getAdminsId } from '../individualStore'

const http = express!

http.use(express.static("client/build", { maxAge: 31557600000 }))

http.get("/api/verify/:id") do(req, res)
	if getPlayersId!.includes(req.params.id)
		res.sendStatus(200)
	else
		res.sendStatus(400)

http.get("/api/verify/admin/:id") do(req, res)
	if getAdminsId!.includes(req.params.id)
		res.sendStatus(200)
	else
		res.sendStatus(400)

http.get(/.*/) do(req,res)
	unless req.accepts(['image/*', 'html']) == 'html'
		return res.sendStatus(404)

	res.send index.body

export default http
