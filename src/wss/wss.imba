import { wsEvents } from './wsEvents'
import { ClientMessage, SERVER_MSG_TYPE, ServerMessage } from './messageTypes'
import _ from 'underscore'
import * as individualStore from '../individualStore'
import * as wsStore from './wsStore'
import WebSocket, { WebSocketServer } from 'ws'
import * as url from 'url'
import crypto from 'crypto'

const wss = new WebSocketServer(
	port: 30030
)

def notifyAdminsOnConnection
	wsStore.sendConnectedClientList individualStore.getAdminsId!

wss.on "connection", do(ws, req)
	const {query: {token}} = url.parse(req.url, true)
	// @ts-ignore
	const clientToken = token.toString()
	const serverToken = crypto.randomBytes(32).toString('hex');

	if individualStore.getAllId!.includes(clientToken)
		// @ts-ignore
		wsStore.add(clientToken, serverToken, ws)
		notifyAdminsOnConnection!
	else
		ws.close!
		
	ws.on "close" do()
		wsStore.remove(clientToken, serverToken)
		notifyAdminsOnConnection!
	
	ws.on "message" do(rawData)
		try 
			const data = new ClientMessage JSON.parse "{rawData}"
			throw new Error "Unable to retrieve type from message" if !data.type

			const res = wsEvents[data.type](data.data)

			if res and res instanceof ServerMessage
				if res.target
					wsStore.send(individualStore.getPlayerAndObserversId(res.target), res)
				else
					wsStore.send(individualStore.getAllEnabledId!, res)
			
		catch error
			console.error("Error: {error}")

	
	




export default wss

