import { IndividualPlayerStore } from './types'
import * as _ from 'underscore'

const playerStore\IndividualPlayerStore[] = [
	# http://192.168.1.12:3000/play/LNPP2WCSQWF7AG6RSMHTEHAZKFYOWT
	new IndividualPlayerStore
		id: "LNPP2WCSQWF7AG6RSMHTEHAZKFYOWT"
		name: "Hugo"
		enabled?: true
		mentalismScore: 100
		investigationTimerSeconds: null

	# http://192.168.1.12:3000/play/2NO2SMEBOQX205CZQ0RIR16QGRN2KB
	new IndividualPlayerStore
		id: "2NO2SMEBOQX205CZQ0RIR16QGRN2KB"
		name: "Bryan"
		enabled?: true
		mentalismScore: 100
		investigationTimerSeconds: null

	# http://192.168.1.12:3000/play/ZZQAZBAJ4YETGJ39INN7YFZL8KRFPP
	new IndividualPlayerStore
		id: "ZZQAZBAJ4YETGJ39INN7YFZL8KRFPP"
		name: "Léo"
		enabled?: true
		mentalismScore: 100
		investigationTimerSeconds: null
]

# http://192.168.1.12:3000/control-panel/YZQf7E5CH9WCRXHSZ5JPO3FBKDM4Q0
const adminStore = [
	id: "YZQf7E5CH9WCRXHSZ5JPO3FBKDM4Q0"
	name: "Léo"
]

def getPlayerState id\string
	_.find(playerStore, do(s) s.id === id)

def getEnabledPlayerStates
	playerStore.filter(do(s) s.enabled?)

def getPlayersId
	playerStore.map(do(s) s.id)

def getAdminsId
	adminStore.map(do(s) s.id)

def getAllEnabledId 
	[
		...(getEnabledPlayerStates!.map(do(s) s.id))
		...getAdminsId!
	]

def getAllId
	[
		...getPlayersId!
		...getAdminsId!
	]

def getPlayerAndObserversId playerId\string
	[
		...[(getPlayerState playerId).id if getPlayerState playerId]
		...getAdminsId!
	]

def get id\string
	const state = getPlayerState id

	JSON.parse JSON.stringify new IndividualPlayerStore
		id: state.id
		name: state.name
		enabled?: state.enabled?
		mentalismScore: state.mentalismScore
		investigationTimerSeconds: state.investigationTimerSeconds

def enable id\string
	const playerState = getPlayerState id
	playerState.enabled? = true

def disable id\string
	const playerState = getPlayerState id
	playerState.enabled? = false

def setMentalismScore id\string, score\Number
	const playerState = getPlayerState id
	playerState.mentalismScore = score

def setInvestigationTimerSeconds id\string, seconds\Number
	const playerState = getPlayerState id
	playerState.investigationTimerSeconds = seconds

	if !seconds
		clearInterval playerState.investigationTimerInterval

const investigationTimerIntervalHandler = do(individualStore)
	if individualStore.investigationTimerSeconds and individualStore.investigationTimerSeconds > 0
		individualStore.investigationTimerSeconds -= 1

def startInvestigationTimerInterval id\string
	const playerState = getPlayerState id

	if playerState.investigationTimerSeconds
		playerState.investigationTimerInterval = setInterval (do investigationTimerIntervalHandler playerState), 1000 

def stopInvestigationTimerInterval id\string
	const playerState = getPlayerState id
	clearInterval playerState.investigationTimerInterval

export {
	get
	enable
	disable
	setMentalismScore
	setInvestigationTimerSeconds
	startInvestigationTimerInterval
	stopInvestigationTimerInterval

	getPlayersId
	getAllEnabledId
	getAdminsId
	getAllId
	getPlayerAndObserversId
}
