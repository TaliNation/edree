import { SERVER_MSG_TYPE, ServerMessage } from './messageTypes'
import _ from 'underscore'

class WsConnection
	constructor clientToken\string, serverToken\string, ws\WebSocket
		this.clientToken\string = clientToken
		this.ws\WebSocket = ws
		this.serverToken\string = serverToken

let clients\WsConnection[] = []

def getAll
	clients

def get clientToken\string
	clients.filter do(c) c.clientToken === clientToken

def getMultiple clientTokens\string[]
	clients.filter do(c) clientTokens.includes(c.clientToken)


def add clientToken\string, serverToken\string, ws\WebSocket
	clients.push(new WsConnection(clientToken, serverToken, ws))

def remove clientToken\string, serverToken\string
	clients = clients.filter do(c) !(c.clientToken === clientToken and c.serverToken === serverToken)

def send clientTokens\string[], message\ServerMessage
	const sockets = getMultiple clientTokens
	for socket in sockets
		socket.ws.send JSON.stringify message

def sendConnectedClientList clientTokens\string[]
	send clientTokens, new ServerMessage
		type: SERVER_MSG_TYPE.CLIENT_CONNECTION
		data: getAll!.map(do(c) c.clientToken)

export {
	add
	remove
	send
	sendConnectedClientList
}
