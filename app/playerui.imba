import { Gapless5 } from '../assets/js/gapless5'
import { SharedStore, IndividualPlayerStore, CharacterFrame, Room, Song } from '../src/types'
import { PAGE_STATUS, receiveWsMessage, sendWsMessage, triggerAnimation, areEqual } from './helpers'
import { ClientMessage, CLIENT_MSG_TYPE, SERVER_MSG_TYPE } from '../src/wss/messageTypes'
import axios from 'axios'
import localforage from 'localforage'
import lodash from 'lodash'
import '../assets/css/timer.css'

tag PlayerUi
	ws\WebSocket
	pageStatus\string
	token\string

	def routed params
		pageStatus = PAGE_STATUS.LOADING
		token = params.token
		render!

		axios
		.get("/api/verify/{params.token}")
		.then(do()
			ws = new WebSocket("ws://{window.location.hostname}:30030/?token={params.token}")
			pageStatus = PAGE_STATUS.LOADED
		)
		.catch(do()
			pageStatus = PAGE_STATUS.ERROR
		)

	def render
		<self>
			if pageStatus === PAGE_STATUS.LOADED
				<PlayerUiContent ws=ws token=token>
			else
				<div>

tag PlayerUiContent
	prop ws\WebSocket
	prop token\string

	sharedState\SharedStore
	individualState\IndividualPlayerStore

	audioPlayer\Gapless5
	audioPlayerVolume\number = 1
	musicActivated?\boolean = false
	timerLoaded? = false

	getTimerInterval = null

	def mount
		audioPlayer = new Gapless5()
		audioPlayer.hasGUI = false

		getTimerIntervalHandler = do
			sendWsMessage ws, new ClientMessage
				type: CLIENT_MSG_TYPE.GET_TIMER
				senderToken: token
				data:
					target: token

		ws.onopen = do()
			sendWsMessage ws, new ClientMessage
				type: CLIENT_MSG_TYPE.GET_SHARED_STATE
				senderToken: token

			sendWsMessage ws, new ClientMessage
				type: CLIENT_MSG_TYPE.GET_INDIVIDUAL_STATE
				senderToken: token
				data: token
			
			sendWsMessage ws, new ClientMessage
				type: CLIENT_MSG_TYPE.GET_TIMER
				senderToken: token
				data:
					target: token

		ws.onmessage = do(rawMessage)
			const message = receiveWsMessage(rawMessage.data)

			if message.type === SERVER_MSG_TYPE.UPDATE_SHARED_STATE
				const newSharedState = new SharedStore message.data
				updateSharedState newSharedState
			elif message.type === SERVER_MSG_TYPE.UPDATE_INDIVIDUAL_STATE
				const newIndividualState = new IndividualPlayerStore message.data
				updateIndividualState newIndividualState
			elif message.type === SERVER_MSG_TYPE.UPDATE_TIMER
				if message.data and !timerLoaded?
					getTimerInterval = setInterval getTimerIntervalHandler, 1000
					timerLoaded? = true
				elif !message.data and timerLoaded?
					clearInterval getTimerInterval
					timerLoaded? = false

				individualState.investigationTimerSeconds = message.data
				console.log individualState.investigationTimerSeconds

			imba.commit!

	def unmount
		audioPlayer.stop!
		audioPlayer = null

	def updateIndividualState newState\IndividualPlayerStore
		individualState = newState

	def updateSharedState newState\SharedStore
		changeMode newState if !sharedState or !areEqual(sharedState.inCase?, newState.inCase?)
		changeRoom newState if !sharedState or !areEqual(sharedState.background, newState.background)
		changeCharacter newState if !sharedState or !areEqual(sharedState.character, newState.character)
		changeMusic newState if musicActivated? and (!sharedState or !areEqual(sharedState.song, newState.song))
		sharedState = newState

	def changeMode newState\SharedStore
		$paperLeft.style.backgroundImage = "url(\"{import("../assets/images/paper-left.png")}\")"
		$paperRight.style.backgroundImage = "url(\"{import("../assets/images/paper-right.png")}\")"

	def changeRoom state\SharedStore
		$background.style.backgroundImage = "url({state.background.file})"

	def changeCharacter state\SharedStore
		if !state.character
			$characterFrame.src = ""
		else
			$characterFrame.src = state.character.file
			if state.inTrial?
				if lodash.sample([true, false])
					triggerAnimation $characterFrame, "hard-appearance-left", 10, "ease-out"
				else
					triggerAnimation $characterFrame, "hard-appearance-right", 10, "ease-out"
			else
				triggerAnimation $characterFrame, "soft-appearance", 0.75, "ease-out"

	def changeMusic state\SharedStore	
		const fadeoutAudio = do
			if audioPlayerVolume > 0.1
				audioPlayerVolume -= 0.1
				audioPlayer.setVolume(audioPlayerVolume - 0.1)
			else
				audioPlayerVolume = 0
				audioPlayer.setVolume(0)		

		const fadeoutAudioInterval = setInterval(fadeoutAudio, 50)

		const getLoopingAudio = do()
			localforage.getItem("{state.song.loopingFile}").then do(cachedAudioBlob)
				if (cachedAudioBlob)
					const loopingFile = URL.createObjectURL(cachedAudioBlob)
					for i in [1 .. 5]
						setTimeout((do() audioPlayer.addTrack loopingFile), 1000)
				else
					window.fetch(state.song.loopingFile).then do(fetchedResult)
						fetchedResult.blob().then do(audioBlob)
							localforage.setItem("{state.song.loopingFile}", audioBlob)
							const loopingFile = URL.createObjectURL(audioBlob)
							for i in [1 .. 5]
								setTimeout((do() audioPlayer.addTrack loopingFile), 1000)

		const setSong = do(audioBlob\Blob)
			const introFile = URL.createObjectURL(audioBlob)
			audioPlayer.addTrack introFile
			audioPlayerVolume = 1
			audioPlayer.setVolume 1
			audioPlayer.play!
			if state.song.loopingFile
				audioPlayer.loop = false
				getLoopingAudio!
			else
				audioPlayer.loop = true
			return

		const stopSong = do()
			audioPlayer.stop!
			audioPlayer.removeAllTracks true

		const changeMusic = do()
			clearInterval(fadeoutAudioInterval)
			stopSong!
			if state.song
				localforage.getItem("{state.song.file}").then do(cachedAudioBlob)
					if (cachedAudioBlob)
						setSong(cachedAudioBlob)
					else
						window.fetch(state.song.file).then do(fetchedResult)
							fetchedResult.blob().then do(audioBlob)
								localforage.setItem("{state.song.file}", audioBlob)
								setSong(audioBlob)
						
		setTimeout(changeMusic, 500)

	def activateMusic
		musicActivated? = true
		changeMusic sharedState

	css .bg bgr:no-repeat bgs:cover h:100vh opacity:1.0 transition:background 1.25s linear
	css .timer pos:absolute zi:3
	css .timer@640 fs:48px ml:25px mt:15px
	css .timer@1280 fs:60px ml:35px mt:20px
	css .timer@!640 fs:36px ml:15px mt:10px
	css .paper bgr:no-repeat bgs:100% 100% min-height:200px pos:absolute zi:2
	css .paper-left l:0 b:0 h:33% w:50%
	css .paper-right r:0 t:0 h:32% w:60% ta:right
	css .character-frame ta:center zi:1 pos:relative h:100% d:flex align-items:center
		img h:75% m:auto pt:0 pb:30px
	css .music-button fs:22px p:10px
	css .song-name fs:22px p:10px
	css .paper-left-content d:flex h:100% ai:flex-end
	css .mentalism-score c:#222
	css .mentalism-score@640 fs:48px ml:25px mb:15px
	css .mentalism-score@1280 fs:60px ml:35px mb:20px
	css .mentalism-score@!640 fs:36px ml:15px mb:10px

	def render
		<self>
			<div$background.bg>
				if individualState and individualState.investigationTimerSeconds !== null and individualState.investigationTimerSeconds !== undefined
					<div.timer>
						<Timer secondsLeft=individualState.investigationTimerSeconds>

				if sharedState and sharedState.character
					<div.character-frame>
						<img$characterFrame>

				<div$paperLeft.paper.paper-left>
					<div.paper-left-content>
						<span.mentalism-score> "{(individualState ? individualState.mentalismScore : "100")} / 100"
				<div$paperRight.paper.paper-right>
					if musicActivated?
						<p.song-name> " {sharedState.song.title unless !sharedState or !sharedState.song} 🎵"
					else
						<button.btn.btn-light.music-button[bgc:#735bc1 c:#fff zi:1] @click=activateMusic> "Activer la musique"

tag Timer
	prop secondsLeft\number
	
	TIME_LIMIT = 2700
	FULL_DASH_ARRAY = 283
	COLOR_CODES\any

	def mount
		this.COLOR_CODES =
			info:
				color: "green"
			warning:
				color: "orange"
				threshold: 900
			alert:
				color: "red"
				threshold: 300

	get timeString
		let minutes = Math.floor(secondsLeft / 60)
		let secondsInMinute = secondsLeft % 60
		
		minutes = "0{minutes}" if minutes < 10
		secondsInMinute = "0{secondsInMinute}" if secondsInMinute < 10
		"{minutes}:{secondsInMinute}"
	
	get remainingPathColor
		const { alert, warning, info } = this.COLOR_CODES

		if secondsLeft <= alert.threshold
			alert.color
		elif secondsLeft <= warning.threshold
			warning.color
		else
			info.color
	
	get timeFraction
		(secondsLeft / this.TIME_LIMIT) - (1 / this.TIME_LIMIT) * (1 - (secondsLeft / this.TIME_LIMIT))

	def render
		<self>
			<div.base-timer>
				<svg.base-timer__svg viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg">
					<g.base-timer__circle>
						# <rect.base-timer__path-elapsed width="100" height="100">
						<circle.base-timer__path-elapsed cx="50" cy="50" r="45" />
						if this.COLOR_CODES
							<path#base-timer-path-remaining.base-timer__path-remaining.{remainingPathColor}
								stroke-dasharray="{(timeFraction * this.FULL_DASH_ARRAY).toFixed(0)} 283" 
								d="M 50, 50 m -45, 0 a 45,45 0 1,0 90,0 a 45,45 0 1,0 -90,0">

				<span#base-timer-label.base-timer__label> timeString

export {
	PlayerUi
}
