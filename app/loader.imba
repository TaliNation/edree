import { Song, Room, CharacterFrame } from '../src/types'

const CHARACTER_TYPE = 
	PLAYER: "player"
	MONOKUMA: "monokuma"

const CHARACTER_FRAMES = 
	aurore:
		file: import("../assets/images/frames/aurore.png")
		name: "Aurore"
		type: CHARACTER_TYPE.PLAYER

	ayumi:
		file: import("../assets/images/frames/ayumi.png")
		name: "Ayumi"
		type: CHARACTER_TYPE.PLAYER

	coach:
		file: import("../assets/images/frames/coach.png")
		name: "Coach"
		type: CHARACTER_TYPE.PLAYER

	eleonore:
		file: import("../assets/images/frames/eleonore.png")
		name: "Eleonore"
		type: CHARACTER_TYPE.PLAYER
	
	emmanuelle:
		file: import("../assets/images/frames/emmanuelle.png")
		name: "Emmanuelle"
		type: CHARACTER_TYPE.PLAYER
	
	henry:
		file: import("../assets/images/frames/henry.png")
		name: "Henry"
		type: CHARACTER_TYPE.PLAYER
	
	leonie:
		file: import("../assets/images/frames/leonie.png")
		name: "Leonie"
		type: CHARACTER_TYPE.PLAYER
	
	leslie:
		file: import("../assets/images/frames/leslie.png")
		name: "Leslie"
		type: CHARACTER_TYPE.PLAYER

	marika:
		file: import("../assets/images/frames/marika.png")
		name: "Marika"
		type: CHARACTER_TYPE.PLAYER

	marius: 
		file: import("../assets/images/frames/marius.png")
		name: "Marius"
		type: CHARACTER_TYPE.PLAYER
	
	natasha: 
		file: import("../assets/images/frames/natasha.png")
		name: "Natasha"
		type: CHARACTER_TYPE.PLAYER

	nick:
		file: import("../assets/images/frames/nick.png")
		name: "Nick"
		type: CHARACTER_TYPE.PLAYER

	stacy:
		file: import("../assets/images/frames/stacy.png")
		name: "Stacy"
		type: CHARACTER_TYPE.PLAYER

	warren:
		file: import("../assets/images/frames/warren.png")
		name: "Warren"
		type: CHARACTER_TYPE.PLAYER

	monokuma_angry:
		file: import("../assets/images/frames/monokuma.angry.png")
		name: "Monokuma Angry"
		type: CHARACTER_TYPE.MONOKUMA
	
	monokuma_gahaha:
		file: import("../assets/images/frames/monokuma.gahaha.png")
		name: "Monokuma Gahaha"
		type: CHARACTER_TYPE.MONOKUMA

	monokuma_kyubuh:
		file: import("../assets/images/frames/monokuma.kyubuh.png")
		name: "Monokuma Kyubuh"
		type: CHARACTER_TYPE.MONOKUMA

	monokuma_normal:
		file: import("../assets/images/frames/monokuma.normal.png")
		name: "Monokuma Normal"
		type: CHARACTER_TYPE.MONOKUMA

	monokuma_panting:
		file: import("../assets/images/frames/monokuma.panting.png")
		name: "Monokuma Panting"
		type: CHARACTER_TYPE.MONOKUMA

	monokuma_pupupu:
		file: import("../assets/images/frames/monokuma.pupupu.png")
		name: "Monokuma Pupupu"
		type: CHARACTER_TYPE.MONOKUMA

	monokuma_what:
		file: import("../assets/images/frames/monokuma.what.png")
		name: "Monokuma What"
		type: CHARACTER_TYPE.MONOKUMA

const LOCATIONS =
	COMMON: "common"
	OTHER: "other"
	AREA1: "area1"

const ROOMS =
	arcade:
		file: import("../assets/images/bg/arcade.jpg")
		name: "Arcade"
		location: LOCATIONS.COMMON

	bar:
		file: import("../assets/images/bg/bar.jpg")
		name: "Bar"
		location: LOCATIONS.AREA1

	bedroom:
		file: import("../assets/images/bg/bedroom.jpg")
		name: "Bedroom"
		location: LOCATIONS.COMMON

	canteen:
		file: import("../assets/images/bg/canteen.jpg")
		name: "Canteen"
		location: LOCATIONS.COMMON

	court:
		file: import("../assets/images/bg/court.png")
		name: "Court"
		location: LOCATIONS.OTHER

	dark:
		file: import("../assets/images/bg/dark.jpg")
		name: "Dark"
		location: LOCATIONS.OTHER

	dorm:
		file: import("../assets/images/bg/dorm.jpg")
		name: "Dorm"
		location: LOCATIONS.COMMON
	
	elevator:
		file: import("../assets/images/bg/elevator.jpg")
		name: "Elevator"
		location: LOCATIONS.OTHER
	
	kitchen:
		file: import("../assets/images/bg/kitchen.jpg")
		name: "Kitchen"
		location: LOCATIONS.COMMON

	locker:
		file: import("../assets/images/bg/locker.jpg")
		name: "Locker"
		location: LOCATIONS.COMMON
	
	lounge:
		file: import("../assets/images/bg/lounge.jpg")
		name: "Lounge"
		location: LOCATIONS.AREA1
	
	sauna:
		file: import("../assets/images/bg/sauna.jpg")
		name: "Sauna"
		location: LOCATIONS.AREA1
	
	shower:
		file: import("../assets/images/bg/shower.jpg")
		name: "Shower"
		location: LOCATIONS.COMMON
	
	theater:
		file: import("../assets/images/bg/theater.jpg")
		name: "Theater"
		location: LOCATIONS.COMMON
	
	warehouse:
		file: import("../assets/images/bg/warehouse.jpg")
		name: "Warehouse"
		location: LOCATIONS.COMMON

const SONG_TYPE =
	NORMAL: "normal"
	SPECIAL: "special"
	TRIAL: "trial"

const SONGS =
	a_race_against_the_devil:
		# @ts-ignore
		file: import("../assets/audio/bgm/a_race_against_the_devil.mp3")
		title: "A Race Against the Devil"
		type: SONG_TYPE.SPECIAL

	absolute_boredom:
		# @ts-ignore
		file: import("../assets/audio/bgm/absolute_boredom.mp3")
		title: "Absolute Boredom"
		type: SONG_TYPE.NORMAL
	
	all_time_homies:
		# @ts-ignore
		file: import("../assets/audio/bgm/all_time_homies.mp3")
		title: "All Time Homies"
		type: SONG_TYPE.NORMAL

	another_reason_to_kill_each_other:
		# @ts-ignore
		file: import("../assets/audio/bgm/another_reason_to_kill_each_other.mp3")
		title: "Another Reason to Kill Each Other"
		type: SONG_TYPE.SPECIAL

	battle_of_wits:
		# @ts-ignore
		file: import("../assets/audio/bgm/battle_of_wits.mp3")
		title: "Battle of Wits"
		type: SONG_TYPE.NORMAL

	battle_of_fools:
		# @ts-ignore
		file: import("../assets/audio/bgm/battle_of_fools.mp3")
		title: "Battle of Fools"
		type: SONG_TYPE.NORMAL

	bring_in_the_defendants:
		# @ts-ignore
		file: import("../assets/audio/bgm/bring_in_the_defendants.mp3")
		title: "Bring in the Defendants"
		type: SONG_TYPE.TRIAL

	class_trial_blatant_lies:
		# @ts-ignore
		file: import("../assets/audio/bgm/class_trial_blatant_lies.mp3")
		title: "Class Trial - Blatant Lies"
		type: SONG_TYPE.TRIAL

	class_trial_chaotic_debate:
		# @ts-ignore
		file: import("../assets/audio/bgm/class_trial_chaotic_debate.mp3")
		title: "Class Trial - Chaotic Debate"
		type: SONG_TYPE.TRIAL
	
	class_trial_civilised_debate:
		# @ts-ignore
		file: import("../assets/audio/bgm/class_trial_civilised_debate.mp3")
		title: "Class Trial - Civilised Debate"
		type: SONG_TYPE.TRIAL

	# class_trial_confusion:
	# 	# @ts-ignore
	# 	file: import("../assets/audio/bgm/class_trial_confusion.mp3")
	# 	title: "Class Trial - Confusion"
	# 	type: SONG_TYPE.TRIAL

	class_trial_smarty_pants:
		# @ts-ignore
		file: import("../assets/audio/bgm/class_trial_smarty_pants.mp3")
		title: "Class Trial - Smarty Pants"
		type: SONG_TYPE.TRIAL

	class_trial_target_in_sight:
		# @ts-ignore
		file: import("../assets/audio/bgm/class_trial_target_in_sight.mp3")
		title: "Class Trial - Target in Sight"
		type: SONG_TYPE.TRIAL
	
	class_trial_the_moment_of_truth:
		# @ts-ignore
		file: import("../assets/audio/bgm/class_trial_the_moment_of_truth.mp3")
		title: "Class Trial - The Moment of Truth"
		type: SONG_TYPE.TRIAL

	class_trial_thought_provocation:
		# @ts-ignore
		file: import("../assets/audio/bgm/class_trial_thought_provocation.mp3")
		title: "Class Trial - Thought Provocation"
		type: SONG_TYPE.TRIAL

	class_trial_turn_of_events:
		# @ts-ignore
		file: import("../assets/audio/bgm/class_trial_turn_of_events.mp3")
		title: "Class Trial - Turn of Events"
		type: SONG_TYPE.TRIAL

	class_trial_wait_a_second:
		# @ts-ignore
		file: import("../assets/audio/bgm/class_trial_wait_a_second.mp3")
		title: "Class Trial - Wait a Second"
		type: SONG_TYPE.TRIAL

	closer_look:
		# @ts-ignore
		file: import("../assets/audio/bgm/closer_look.mp3")
		title: "Closer Look"
		type: SONG_TYPE.SPECIAL

	cold_sweats_of_akwardness:
		# @ts-ignore
		file: import("../assets/audio/bgm/cold_sweats_of_akwardness.mp3")
		title: "Cold Sweats of Akwardness"
		type: SONG_TYPE.NORMAL

	conspiracies:
		# @ts-ignore
		file: import("../assets/audio/bgm/conspiracies.mp3")
		title: "Conspiracies"
		type: SONG_TYPE.NORMAL

	dotdotdot:
		# @ts-ignore
		file: import("../assets/audio/bgm/dotdotdot.mp3")
		title: "..."
		type: SONG_TYPE.SPECIAL

	ekustreamu:
		# @ts-ignore
		file: import("../assets/audio/bgm/ekustreamu.mp3")
		title: "EKUSTREAMU"
		type: SONG_TYPE.TRIAL

	game_on:
		# @ts-ignore
		file: import("../assets/audio/bgm/game_on.mp3")
		title: "Game On"
		type: SONG_TYPE.SPECIAL

	glorious_everyday:
		# @ts-ignore
		file: import("../assets/audio/bgm/glorious_everyday.mp3")
		title: "Glorious Everyday"
		type: SONG_TYPE.NORMAL

	god_will_be_watching:
		# @ts-ignore
		file: import("../assets/audio/bgm/god_will_be_watching.mp3")
		title: "God Will Be Watching"
		type: SONG_TYPE.SPECIAL

	higher_society:
		# @ts-ignore
		file: import("../assets/audio/bgm/higher_society.mp3")
		title: "Higher Society"
		type: SONG_TYPE.NORMAL

	hot_springs_and_soy_milk:
		# @ts-ignore
		file: import("../assets/audio/bgm/hot_springs_and_soy_milk.mp3")
		title: "Hot Springs and Soy Milk"
		type: SONG_TYPE.NORMAL

	investigation_time:
		# @ts-ignore
		file: import("../assets/audio/bgm/investigation_time.mp3")
		title: "Investigation Time"
		type: SONG_TYPE.TRIAL

	living_walls:
		# @ts-ignore
		file: import("../assets/audio/bgm/living_walls.mp3")
		title: "Living Walls"
		type: SONG_TYPE.NORMAL

	monokuma_moment:
		# @ts-ignore
		file: import("../assets/audio/bgm/monokuma_moment.mp3")
		title: "Monokuma Moment"
		type: SONG_TYPE.NORMAL

	morning_routine:
		// @ts-ignore
		file: import("../assets/audio/bgm/morning_routine.mp3")
		title: "Morning Routine"
		type: SONG_TYPE.NORMAL

	never_lose_hope:
		# @ts-ignore
		file: import("../assets/audio/bgm/never_lose_hope.mp3")
		title: "Never Lose Hope"
		type: SONG_TYPE.SPECIAL

	open_heart_wound:
		# @ts-ignore
		file: import("../assets/audio/bgm/open_heart_wound.mp3")
		title: "Open Heart Wound"
		type: SONG_TYPE.SPECIAL

	pain:
		# @ts-ignore
		file: import("../assets/audio/bgm/pain.mp3")
		title: "Pain"
		type: SONG_TYPE.NORMAL

	pdd:
		# @ts-ignore
		file: import("../assets/audio/bgm/pdd.mp3")
		title: "PDD"
		type: SONG_TYPE.NORMAL

	restless:
		# @ts-ignore
		file: import("../assets/audio/bgm/restless.mp3")
		title: "Restless"
		type: SONG_TYPE.NORMAL

	small_talks:
		# @ts-ignore
		file: import("../assets/audio/bgm/small_talks.mp3")
		title: "Small Talks"
		type: SONG_TYPE.NORMAL

	starless_sky:
		# @ts-ignore
		file: import("../assets/audio/bgm/starless_sky.mp3")
		title: "Starless Sky"
		type: SONG_TYPE.NORMAL

	stopping_time:
		# @ts-ignore
		file: import("../assets/audio/bgm/stopping_time.mp3")
		title: "Stopping Time"
		type: SONG_TYPE.SPECIAL

	suffocation:
		# @ts-ignore
		file: import("../assets/audio/bgm/suffocation.mp3")
		title: "Suffocation"
		type: SONG_TYPE.SPECIAL

	sunless_sky:
		# @ts-ignore
		file: import("../assets/audio/bgm/sunless_sky.mp3")
		title: "Sunless Sky"
		type: SONG_TYPE.NORMAL

	the_dark_descent:
		# @ts-ignore
		file: import("../assets/audio/bgm/the_dark_descent.mp3")
		title: "The Dark Descent"
		type: SONG_TYPE.TRIAL

	the_past:
		# @ts-ignore
		file: import("../assets/audio/bgm/the_past.mp3")
		title: "The Past"
		type: SONG_TYPE.SPECIAL

	the_rules_are_simple:
		# @ts-ignore
		file: import("../assets/audio/bgm/the_rules_are_simple.mp3")
		title: "The Rules are Simple"
		type: SONG_TYPE.TRIAL

	# the_sky_and_the_world:
	# 	# @ts-ignore
	# 	file: import("../assets/audio/bgm/the_sky_and_the_world.mp3")
	# 	title: "The Sky and the World"
	# 	type: SONG_TYPE.SPECIAL

	ultimate_despair_wailing:
		# @ts-ignore
		file: import("../assets/audio/bgm/the_ultimate_despair_wailing.mp3")
		title: "Ultimate Despair Wailing"
		type: SONG_TYPE.TRIAL

	ultimate_despair:
		# @ts-ignore
		file: import("../assets/audio/bgm/the_ultimate_despair.mp3")
		title: "Ultimate Despair"
		type: SONG_TYPE.NORMAL

	widening_fracture:
		# @ts-ignore
		file: import("../assets/audio/bgm/widening_fracture.mp3")
		title: "Widening Fracture"
		type: SONG_TYPE.SPECIAL

	you_and_me_and_the_others:
		# @ts-ignore
		file: import("../assets/audio/bgm/you_and_me_and_the_others.mp3")
		title: "You and Me and the Others"
		type: SONG_TYPE.SPECIAL
	
export {
	CHARACTER_FRAMES, CHARACTER_TYPE
	ROOMS, LOCATIONS
	SONGS, SONG_TYPE
}

