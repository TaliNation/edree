import { SERVER_MSG_TYPE, ServerMessage, CLIENT_MSG_TYPE } from './messageTypes'
import * as sharedStore from '../sharedStore'
import * as individualStore from '../individualStore'
import * as wsStore from './wsStore'

const updateSharedState = do()
	new ServerMessage 
		type: SERVER_MSG_TYPE.UPDATE_SHARED_STATE
		data: sharedStore.get!

const updateIndividualState = do(target)
	new ServerMessage 
		type: SERVER_MSG_TYPE.UPDATE_INDIVIDUAL_STATE
		data: individualStore.get target
		target: target

const updateTimer = do(target)
	new ServerMessage 
		type: SERVER_MSG_TYPE.UPDATE_TIMER
		data: individualStore.get(target).investigationTimerSeconds
		target: target

def displayCharacter data
	sharedStore.setCharacter data
	updateSharedState!

def changeRoom data
	sharedStore.setBackground data
	updateSharedState!

def changeMusic data
	sharedStore.setSong data
	updateSharedState!

def changeCaseMode data
	sharedStore.setInCaseMode data
	updateSharedState!

def changeTrialMode data
	sharedStore.setInTrialMode data
	updateSharedState!

def enablePlayer data
	individualStore.enable data
	new ServerMessage
		type: SERVER_MSG_TYPE.UPDATE_SHARED_STATE
		data: sharedStore.get!
		target: data

def disablePlayer data
	individualStore.disable data
	new ServerMessage
		type: SERVER_MSG_TYPE.UPDATE_SHARED_STATE
		data: sharedStore.getDefault!
		target: data

def changePlayerMentalismScore data
	individualStore.setMentalismScore data.target, data.score
	updateIndividualState data.target

def getEveryIndividualStates data
	const ids = individualStore.getPlayersId!
	new ServerMessage 
		type: SERVER_MSG_TYPE.UPDATE_EVERY_INDIVIDUAL_STATES
		data: ids.map do(id) individualStore.get id
		target: individualStore.getAdminsId![0]

def setTimer data
	individualStore.setInvestigationTimerSeconds data.target, data.minutes * 60
	updateTimer data.target

def removeTimer data
	individualStore.setInvestigationTimerSeconds data.target, null
	updateTimer data.target

def startTimer data
	individualStore.startInvestigationTimerInterval data.target
	updateTimer data.target

def stopTimer data
	individualStore.stopInvestigationTimerInterval data.target
	updateTimer data.target

def getTimer data
	updateTimer data.target

const wsEvents = {
	[CLIENT_MSG_TYPE.DISPLAY_CHARACTER]: displayCharacter
	[CLIENT_MSG_TYPE.CHANGE_ROOM]: changeRoom
	[CLIENT_MSG_TYPE.GET_SHARED_STATE]: updateSharedState
	[CLIENT_MSG_TYPE.CHANGE_MUSIC]: changeMusic
	[CLIENT_MSG_TYPE.ENABLE]: enablePlayer
	[CLIENT_MSG_TYPE.DISABLE]: disablePlayer
	[CLIENT_MSG_TYPE.GET_INDIVIDUAL_STATE]: updateIndividualState
	[CLIENT_MSG_TYPE.CHANGE_MENTALISM_SCORE]: changePlayerMentalismScore
	[CLIENT_MSG_TYPE.GET_EVERY_INDIVIDUAL_STATES]: getEveryIndividualStates
	[CLIENT_MSG_TYPE.SWITCH_CASE_MODE]: changeCaseMode
	[CLIENT_MSG_TYPE.SWITCH_TRIAL_MODE]: changeTrialMode

	[CLIENT_MSG_TYPE.SET_TIMER]: setTimer
	[CLIENT_MSG_TYPE.START_TIMER]: startTimer
	[CLIENT_MSG_TYPE.STOP_TIMER]: stopTimer
	[CLIENT_MSG_TYPE.REMOVE_TIMER]: removeTimer
	[CLIENT_MSG_TYPE.GET_TIMER]: getTimer
}

export {
	wsEvents
}
