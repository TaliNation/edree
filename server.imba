import http from './src/http/http'
import wss from './src/wss/wss'

imba.serve http.listen(process.env.PORT or 3000)
